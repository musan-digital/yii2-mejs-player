<?php

namespace lafa\assets;

class MejsDownloadAsset extends \yii\web\AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/';
    public $js = [
        (YII_DEBUG) ? 'download/download.js' : 'download/download.min.js',
    ];
    public $css = [
        (YII_DEBUG) ? 'download/download.css' : 'download/download.min.css',
    ];
    public $depends = [
        'lafa\assets\MejsAsset'
    ];
}
