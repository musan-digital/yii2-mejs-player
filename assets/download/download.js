(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
'use strict';

/**
 * Progress Title
 *
 * This feature enables the displaying of a Progress Title button in the control bar, which basically pauses the media and rewinds
 * it to the initial position.
 */

// Feature configuration

Object.assign(mejs.MepDefaults, {
	/**
  * @type {?String}
  */
	canDownload: false
});

Object.assign(MediaElementPlayer.prototype, {

	/**
  * Feature constructor.
  *
  * Always has to be prefixed with `build` and the name that will be used in MepDefaults.features list
  * @param {MediaElementPlayer} player
  * @param {$} controls
  * @param {$} layers
  * @param {HTMLElement} media
  */
	builddownload: function builddownload(player, controls, layers, media) {
		var t = this,
            downloadTitle = mejs.Utils.isString(t.options.downloadTitle) ? t.options.downloadTitle : mejs.i18n.t('mejs.downloadtitle');

        var icon = '<svg><path fill="#D8D8D8" d="M17,2c2.179,0,4.446,1.587,5.777,4.044l0.653,1.206l1.36-0.175C25.181,7.025,25.587,7,26,7c2.415,0,5,2.813,5,7c0,2.337-2.663,5-5,5H6.606l-0.388-0.043C3.449,18.652,2,16.735,2,15c0-2.922,1.681-4.785,4.496-4.982l1.45-0.102l0.351-1.411C9.293,4.493,12.628,2,17,2 M17,0C11.555,0,7.535,3.276,6.355,8.022C2.42,8.299,0,11.117,0,15c0,2.844,2.274,5.535,6,5.945V21h20c3.418,0,7-3.582,7-7c0-5.418-3.582-9-7-9c-0.505,0-0.993,0.031-1.464,0.092C22.851,1.98,19.935,0,17,0L17,0z"/><polygon fill="#D8D8D8" points="15.992,13.021 15.992,4.61 15.992,4 17.992,4 17.992,4.61 17.992,13.016 22,13.003 17.043,18.051 12,13.034 "/></svg>';

        player.downloadButton = document.createElement('div');

        player.downloadButton.className = t.options.classPrefix + "button " + t.options.classPrefix + "download-button " + t.options.classPrefix + "download";
        player.downloadButton.innerHTML = '<a class="'+ t.options.classPrefix + 'download-link" href="#">' + icon + '</a>';

        t.addControlElement(player.downloadButton, 'download');

        media.addEventListener('loadedmetadata', function () {
            if (!player.isRadio()) {
                if (player.songData != 'undefined') {
                    $(player.downloadButton).show().find('a').attr('href', player.songData.download);
                }
            }
            else {
                $(player.downloadButton).hide();
            }
        }, false);
	}

});

},{}]},{},[1]);
