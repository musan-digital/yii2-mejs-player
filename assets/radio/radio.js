(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
'use strict';

/**
 * Progress Title
 *
 * This feature enables the displaying of a Progress Title button in the control bar, which basically pauses the media and rewinds
 * it to the initial position.
 */

// Feature configuration

Object.assign(mejs.MepDefaults, {
	/**
  * @type {?String}
  */
	radioTitle: null,
    radioSrc: null
});

Object.assign(MediaElementPlayer.prototype, {

	/**
  * Feature constructor.
  *
  * Always has to be prefixed with `build` and the name that will be used in MepDefaults.features list
  * @param {MediaElementPlayer} player
  * @param {$} controls
  * @param {$} layers
  * @param {HTMLElement} media
  */
	buildradio: function buildradio(player, controls, layers, media) {
		var t = this,
            radioTitle = mejs.Utils.isString(t.options.radioTitle) ? t.options.radioTitle : mejs.i18n.t('mejs.radiotitle');

        mejs.i18n.en["mejs.live-broadcast"] = radioTitle;
        if (mejs.i18n.ru != undefined) mejs.i18n.ru["mejs.live-broadcast"] = radioTitle;

		player.loadRadio = function loadRadio() {
		    if (player.songTitleElement != undefined) player.songTitleElement.remove();
            player.setSrc(this.options.radioSrc);
            player.load();
        };
        player.loadRadio();


        player.loadSong = function loadSong(data) {
            player.songData = data;
            player.setSrc(data.url);
            player.load();
            player.play();
        };

        player.isRadio = function isRadio() {
            var radioSrc = t.options.radioSrc;
            var src = media.currentSrc;
            for (var i = 0; i < radioSrc.length; ++i) {
                if (radioSrc[i].src == src) return true;
            }
            return false;
        };

        media.addEventListener('play', function () {
            if (player.isRadio() == false) {
                if (player.songTitleElement != undefined) player.songTitleElement.remove();
                player.songTitleElement = $('<div class="' + t.options.classPrefix + 'song-title">' + t.songData.artist + '&nbsp;&mdash;&nbsp;' + t.songData.title + '</div>').prependTo($(controls).find('.' + t.options.classPrefix + 'time-rail'));
            }
        }, false);

        media.addEventListener('canplaythrough', function () {
            if (player.isRadio()) {
                $(controls).find('.' + t.options.classPrefix + 'time-slider').hide();
                $('<span class="' + t.options.classPrefix + 'broadcast">' + radioTitle + '</span>').appendTo($(controls).find('.' + t.options.classPrefix + 'time-rail'));
            }
        }, false);

        //player.radioElement = $('<div class="' + t.options.classPrefix + 'title"><span>' + radioTitle + '</span></div>').appendTo(controls);
	},

});

},{}]},{},[1]);
