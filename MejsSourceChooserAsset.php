<?php

namespace lafa\assets;

class MejsSourceChooserAsset extends \yii\web\AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/';
    public $js = [
        (YII_DEBUG) ? 'source-chooser/source-chooser.js' : 'source-chooser/source-chooser.min.js',
    ];
    public $css = [
        (YII_DEBUG) ? 'source-chooser/source-chooser.css' : 'source-chooser/source-chooser.min.css',
    ];
    public $depends = [
        'lafa\assets\MejsAsset'
    ];
}
