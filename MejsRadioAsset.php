<?php

namespace lafa\assets;

class MejsRadioAsset extends \yii\web\AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/';
    public $js = [
        (YII_DEBUG) ? 'radio/radio.js' : 'radio/radio.min.js',
    ];
    public $css = [
        (YII_DEBUG) ? 'radio/radio.css' : 'radio/radio.min.css',
    ];
    public $depends = [
        'lafa\assets\MejsAsset'
    ];
}
