<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace lafa\assets;

class MejsAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/mediaelement/build';
    public $js = [
        (YII_DEBUG) ? 'mediaelement-and-player.js' : 'mediaelement-and-player.min.js',
        'lang/ru.js',
    ];
    public $css = [
        (YII_DEBUG) ? 'mediaelementplayer.css' : 'mediaelementplayer.min.css',
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
