<?php

namespace lafa\assets;

class MejsGoogleAnalyticsAsset extends \yii\web\AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/';
    public $js = [
        (YII_DEBUG) ? 'google-analytics/google-analytics.js' : 'google-analytics/google-analytics.min.js',
    ];
    public $depends = [
        'lafa\assets\MejsAsset'
    ];
}
